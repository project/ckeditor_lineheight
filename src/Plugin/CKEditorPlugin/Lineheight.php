<?php

namespace Drupal\ckeditor_lineheight\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Line Height" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lineheight",
 *   label = @Translation("CKEditor Line Height"),
 *   module = "ckeditor_lineheight"
 * )
 */
class Lineheight extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Maintain backward compatibility with path in previous documentation.
    if (file_exists(DRUPAL_ROOT . '/libraries/ckeditor/plugins/lineheight/plugin.js')) {
      return 'libraries/ckeditor/plugins/lineheight/plugin.js';
    }
    return 'libraries/lineheight/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'lineheight' => [
        'label' => $this->t('Line height'),
        'image' => drupal_get_path('module', 'ckeditor_lineheight') . '/icons/lineheight.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    // Available line height 1px to 72px.
    return [
      'line_height' => join('px;', range(1, 72)) . 'px',
    ];
  }

}
